import Head from 'next/head'
import MainLayout from '../components/layout/main'

export default function Home() {
  return (
    <div>
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainLayout>
          <div style={{height: 900}}>
              Hello
          </div>
      </MainLayout>
    </div>
  )
}
