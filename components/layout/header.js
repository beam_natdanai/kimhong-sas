
import SocialTwinButton from '../socialTwinButton';
import Link from 'next/link';

const Header = () => {
    return (
        <div className="header-menu">
            <div className="menu-container">
                <div className="col-12">
                    <div className="custom-container">
                        <div className="row mb-10">
                            <div className="col-4 text-left">
                                <div style={{paddingTop:'3rem'}}><SocialTwinButton /></div>
                            </div>
                            <div className="col-4 text-center">
                                <span><img className="img-size-3" src="/static/images/logo.png" /></span>
                            </div>
                            <div className="col-4 text-right">
                                <div style={{paddingTop:'3rem'}}>
                                    <span><img src="/static/images/us_flag.png" /> EN</span>
                                </div>
                            </div>
                        </div>
                        <div className="menu-container-center">
                            <Link href={`/`}><a className="font-promary space-near-20 ">หน้าแรก</a></Link>
                            <Link href={`/`}><a className="font-promary space-near-20 ">เกี่ยวกับเรา</a></Link>
                            <Link href={`/`}><a className="font-promary space-near-20 ">ผลิตภัณฑ์</a></Link>
                            <Link href={`/`}><a className="font-promary space-near-20 ">บทความ</a></Link>
                            <Link href={`/`}><a className="font-promary space-near-20 ">ราคายางวันนี้</a></Link>
                            <Link href={`/`}><a className="font-promary space-near-20 ">ติดต่อเรา</a></Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
  
  export default Header