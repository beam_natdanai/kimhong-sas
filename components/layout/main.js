
import Header from './header';
import Footer from './footer';

const MainLayout = (props) => {
  return (
    <div>
        <Header />
            {props.children}
        <Footer />
    </div>
  )
}

export default MainLayout