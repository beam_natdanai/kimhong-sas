


const SocialTwinButton = () => {
    return (
        <div className="row">
            <span className="space-around-1"><img className="img-size-1" src="/static/images/social/icon_face.png" /></span>
            <span className="space-around-1"><img className="img-size-1" src="/static/images/social/icon_ig.png" /></span>
            <span className="space-around-1"><img className="img-size-1" src="/static/images/social/icon_line.png" /></span>
        </div>
    )
  }
  
  export default SocialTwinButton